﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.ViewModel;

namespace WebApplication1.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index()
        {
            return View(new BMIData());
        }
        [HttpPost]
        // GET: BMI
        public ActionResult Index(BMIData data)
        {
            //if (data.身高 < 50 || data.身高 > 200)
            //{
               // ViewBag.HeightError = "身高請輸入50~200的數值";
            //}

            //if (data.體重 < 30 || data.體重 > 150)
           // {
              //  ViewBag.WeightError = "體重請輸入30~150的數值";
            //}

            if (ModelState.IsValid)
            {
                var m_height = data.身高/ 100;
                var result = data.體重 / (m_height * m_height);
                var level = "";

                data.BMI = result;

                if (result < 18.5)
                {
                    level = "過輕";
                }
                else if (18.5 <= result && result < 24)
                {
                    level = "適中";
                }
                else if (24 <= result && result < 27)
                {
                    level = "過胖";
                }
                else if (27 <= result && result < 30)
                {
                    level = "過重";
                }
                else if (30 <= result && result < 35)
                {
                    level = "超重";
                }
                else if (35<=result)
                {
                    level = "沒救了";
                }
                data.Level = level;
            }
            return View(data);
        }
    }
}